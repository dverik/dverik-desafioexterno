/*
* Copyright 2018 Builders
*************************************************************
*Nome     : CustomerServiceTest.java
*Autor    : Erik Paula
*Data     : Mon Oct 01 2018 23:28:00 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.customer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.builders.treinamento.repository.CustomerRepository;
import br.com.builders.treinamento.wrappers.CustomerListWrapper;
import br.com.builders.treinamento.wrappers.CustomerWrapper;

/**
 * The class <code>CustomerServiceTest</code> gathering tests from
 * <code>CustomerService</code>.
 * .
 * @author Erik Paula
 * @version 1.0 01/10/2018
 */ 
@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerServiceTest {

    @Autowired
    private CustomerServices customerServices;
    
    @Autowired
    private CustomerRepository customerRepository;
    
    @Autowired
    private MongoTemplate mongoTemplate;
    
    @Before
    public void setup() throws Exception {
        mongoTemplate.dropCollection(Customer.class);
    }
    
    @After
    public void tearDown() throws Exception {
        mongoTemplate.dropCollection(Customer.class);
    }
    
    @Test
    public void createCustomerTest() throws Exception {
        final Customer testCustomer = getValidCustomer();
        final ResponseEntity<String> response = customerServices.createCustomer(testCustomer);
        
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }
    
    @Test
    public void createCustomerWithSameLoginTest() throws Exception {
        final Customer testCustomer = getValidCustomer();
        customerRepository.save(testCustomer);
        
        final Customer duplicateLoginCustomer = new Customer("553fa88c-4511-445c-b33a-ddff58d76996", "BC7836", 
                "http://www.testbuilders.com.br", "Test Builders", "contato@platformbuilders.com.br");
        
        ResponseEntity<String> response = customerServices.createCustomer(duplicateLoginCustomer);
        assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
        
    }
    
    @Test
    public void updateCustomerTest() throws Exception {
        final Customer testCustomer = getValidCustomer();
        customerRepository.save(testCustomer);
        
        final Customer customerToUpdate = new Customer("553fa88c-4511-445c-b33a-ddff58d76886", "BC4589", 
                "http://www.testbuilders.com", "Test Builders", "contato@platformbuilders.com.br");
        final ResponseEntity<String> response = customerServices.updateCustomer(customerToUpdate.getId(), customerToUpdate);
        
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(customerToUpdate.getBaseUrl(), customerRepository.findById("553fa88c-4511-445c-b33a-ddff58d76886").getBaseUrl());
        assertEquals(customerToUpdate.getCrmId(), customerRepository.findById("553fa88c-4511-445c-b33a-ddff58d76886").getCrmId());
        assertEquals(customerToUpdate.getName(), customerRepository.findById("553fa88c-4511-445c-b33a-ddff58d76886").getName());
    }
    
    @Test
    public void partialUpdateCustomerTest() throws Exception {
        final Customer testCustomer = getValidCustomer();
        customerRepository.save(testCustomer);
        
        final Customer customerToUpdate = new Customer("553fa88c-4511-445c-b33a-ddff58d76886", "C645235", 
                "http://www.platformbuilders.com", "Platform Builders", "contato@platformbuilders.com.br");
        final ResponseEntity<String> response = customerServices.partialCustomerUpdate(customerToUpdate.getId(), customerToUpdate);
        
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(customerToUpdate.getBaseUrl(), customerRepository.findById("553fa88c-4511-445c-b33a-ddff58d76886").getBaseUrl());
        assertEquals(customerToUpdate.getCrmId(), customerRepository.findById("553fa88c-4511-445c-b33a-ddff58d76886").getCrmId());
        assertEquals(customerToUpdate.getName(), customerRepository.findById("553fa88c-4511-445c-b33a-ddff58d76886").getName());
    }
    
    @Test
    public void deleteCustomerTest() {
        final Customer testCustomer = getValidCustomer();
        customerRepository.save(testCustomer);
        
        final ResponseEntity<String> response = customerServices.deleteCustomer(testCustomer.getId());
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertTrue(customerRepository.findById(testCustomer.getId()) == null);

    }
    
    @Test
    public void deleteNonExistingCustomerTest() throws Exception {
        final ResponseEntity<String> response = customerServices.deleteCustomer("553fa88c-4511-445c-b33a-ddff54476886");
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
    
    @Test
    public void getCustomerTest() throws Exception {
        final Customer customer = getValidCustomer();
        customerServices.createCustomer(getValidCustomer());
        final ResponseEntity<CustomerWrapper> response = customerServices.getCustomer("553fa88c-4511-445c-b33a-ddff58d76886");
        
        assertEquals(customer, response.getBody().getCustomer());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
    
    @Test
    public void getNonExistingCustomerTest() throws Exception {
        final ResponseEntity<CustomerWrapper> response = customerServices.getCustomer("553f238c-4511-445c-b33a-ddff58d76886");
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
    
    @Test
    public void getAllCustomersTest() throws Exception {
       final List<Customer> customerList = getValidCustomerList();
       for(final Customer customer : customerList) {
           customerRepository.save(customer);
       }
       
       final ResponseEntity<CustomerListWrapper> response = customerServices.getAllCustomers();
       final CustomerListWrapper customers =  new CustomerListWrapper(response.getBody().getCustomers());
       assertEquals(HttpStatus.OK, response.getStatusCode());
       assertEquals(3, customers.getCustomers().size());
    }
    
    protected Customer getValidCustomer() {
        final Customer customer = new Customer("553fa88c-4511-445c-b33a-ddff58d76886", "C645235", 
                "http://www.platformbuilders.com.br", "Platform Builders", "contato@platformbuilders.com.br");   
        return customer;
    }
    
    protected List<Customer> getValidCustomerList() {
        final List<Customer> customerList = new ArrayList<>(3);
        final Customer customerA = new Customer("553fa88c-4511-445c-b33a-ddff58d76886", "C645235", 
                "http://www.platformbuilders.com.br", "Platform Builders", "contato@platformbuilders.com.br");
        
        final Customer customerB = new Customer("553fa88c-4511-445c-b33a-ddff58d76887", "C645236", 
                "http://www.platformbuilders.com.br", "Builders", "contato@platformbuilders.com");
        
        final Customer customerC = new Customer("553fa88c-4511-445c-b33a-ddff58d76888", "C645237", 
                "http://www.platformbuilders.com.br", "Platform", "contato@platformbuilders.com.pt");
        
        customerList.add(customerA);
        customerList.add(customerB);
        customerList.add(customerC);
        return customerList;
    }
}
