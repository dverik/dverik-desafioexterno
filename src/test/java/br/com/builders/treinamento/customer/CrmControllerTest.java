/*
* Copyright 2018 Builders
*************************************************************
*Nome     : CrmControllerTest.java
*Autor    : Erik Paula
*Data     : Mon Oct 01 2018 23:28:00 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.customer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import br.com.builders.treinamento.repository.CustomerRepository;

/**
 * The class <code>CrmControllerTest</code> gathering tests from
 * <code>CrmController</code>.
 * .
 * @author Erik Paula
 * @version 1.0 01/10/2018
 */ 
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CrmControllerTest {

    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private CustomerRepository customerRepository;
    
    @Autowired
    private MongoTemplate mongoTemplate;
    
    @Before
    public void setup() throws Exception {
        mongoTemplate.dropCollection(Customer.class);
    }
    
    @After
    public void tearDown() throws Exception {
        mongoTemplate.dropCollection(Customer.class);
    }
    
    @Test
    public void shouldCreateCustomerTest() throws Exception {
        final MvcResult result = createCustomer();
        assertEquals(201, result.getResponse().getStatus());
    }
    
    @Test
    public void shouldUpdateCustomerTest() throws Exception {
        createCustomer();
        final Customer customer = customerRepository.findByLogin("contato@platformbuilders.com.br");
        final MvcResult result = mockMvc.perform(put("/customers/" + customer.getId())
                .content("{\"crmId\": \"B175235\", \"baseUrl\": \"http://www.builders.com.br\", "
                        + "\"name\": \"Builders\", \"login\": \"contato@platformbuilders.com.br\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();
        
        assertEquals(200, result.getResponse().getStatus());
    }
    
    @Test
    public void shouldPartialUpdateCustomerTest() throws Exception {
        createCustomer();
        final Customer customer = customerRepository.findByLogin("contato@platformbuilders.com.br");
        final MvcResult result = mockMvc.perform(patch("/customers/" + customer.getId())
                .content("{\"crmId\": \"C738462\", \"baseUrl\": \"http://www.platformbuilders.com.br\", "
                        + "\"name\": \"Platform Builders\", \"login\": \"contato@platformbuilders.com.br\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();
        
        assertEquals(200, result.getResponse().getStatus());
    }
    
    @Test
    public void shouldDeleteCustomerTest() throws Exception {
        createCustomer();
        final Customer customer = customerRepository.findByLogin("contato@platformbuilders.com.br");
        final MvcResult result = mockMvc.perform(delete("/customers/" + customer.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();
        
        assertEquals(200, result.getResponse().getStatus());
        assertTrue(customerRepository.findByLogin("contato@platformbuilders.com.br") == null);
    }
    
    @Test
    public void shouldGetAllCustomersTest() throws Exception {
        insertCustomersOnDatabase();
        final MvcResult result = mockMvc.perform(get("/customers")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();
        
        final JSONObject customersJson = new JSONObject(result.getResponse().getContentAsString());
        final JSONArray customersArray = customersJson.getJSONArray("customers");
        customersArray.length();
        
        assertNotNull(result.getResponse().getContentAsString());
        assertEquals(5, customersArray.length());
        assertTrue(customersArray.get(0).toString().contains("contato@platformbuilders.com.br"));
    }
    
    @Test
    public void shouldGetCustomerByIdTest() throws Exception {
        createCustomer();
        final Customer customer = customerRepository.findByLogin("contato@platformbuilders.com.br");
        final MvcResult result = mockMvc.perform(get("/customers/" + customer.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();
        
        final JSONObject customerJson = new JSONObject(result.getResponse().getContentAsString());
        assertNotNull(customerJson);
        assertEquals("C645235", customerJson.get("crmId"));
        }
    
    private String getCustomerJson() {
        final String json = "{\"crmId\": \"C645235\", \"baseUrl\": \"http://www.platformbuilders.com.br\", "
                + "\"name\": \"Platform Builders\", \"login\": \"contato@platformbuilders.com.br\"}";
        return json;
    }
    
    private MvcResult createCustomer() throws Exception {
        return mockMvc.perform(post("/customers")
                .content(getCustomerJson())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();
    }
    
    private void insertCustomersOnDatabase() throws Exception {
        
        final List<Customer> customerList = new ArrayList<>(5);
        final Customer customerA = new Customer("553fa88c-4511-445c-b33a-ddff58d76886", "C645235", 
                "http://www.platformbuilders.com.br", "Platform Builders", "contato@platformbuilders.com.br");
        
        final Customer customerB = new Customer("553fa88c-4511-445c-b33a-ddff58d76887", "C645236", 
                "http://www.platformbuilders.com.br", "Builders", "contato@platformbuilders.com");
        
        final Customer customerC = new Customer("553fa88c-4511-445c-b33a-ddff58d76888", "C645237", 
                "http://www.platformbuilders.com.br", "Platform", "contato@platformbuilders.com.pt");
        
        final Customer customerD = new Customer("553fa88c-4511-445c-b33a-ddff58d76889", "C645238", 
                "http://www.platformbuilders.com.kr", "Platform", "contato@platformbuilders.com.kr");
        
        final Customer customerE = new Customer("553fa88c-4511-445c-b33a-ddff58d76890", "C645239", 
                "http://www.platformbuilders.com.kp", "Platform", "contato@platformbuilders.com.kp");
        
        customerList.add(customerA);
        customerList.add(customerB);
        customerList.add(customerC);
        customerList.add(customerD);
        customerList.add(customerE);
        
        customerRepository.save(customerList);
    }
}
