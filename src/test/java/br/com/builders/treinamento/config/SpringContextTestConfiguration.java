/*
* Copyright 2018 Builders
*************************************************************
*Nome     : CustomerServiceTest.java
*Autor    : Erik Paula
*Data     : Mon Oct 01 2018 23:28:00 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.config;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * The class <code>SpringContextTestConfiguration</code> represents the test
 * context of Spring Integration Tests
 * .
 * @author Erik Paula
 * @version 1.0 01/10/2018
 */ 
@Configuration
@ComponentScan(basePackages = {"br.com.builders.treinamento.customer", "br.com.builders.treinamento.repository"})
public class SpringContextTestConfiguration {
    
    @Bean
    public RestTemplate restTemplate() {
        return Mockito.mock(RestTemplate.class);
    }

}
