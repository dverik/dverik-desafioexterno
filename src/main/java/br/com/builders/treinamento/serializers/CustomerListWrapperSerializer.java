/*
* Copyright 2018 Builders
*************************************************************
*Nome     : CustomerListWrapperSerializer.java
*Autor    : Erik Paula
*Data     : Mon Oct 03 2018 23:28:00 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.serializers;

import static br.com.builders.treinamento.serializers.SerializationLabel.CUSTOMERS;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import br.com.builders.treinamento.customer.Customer;
import br.com.builders.treinamento.wrappers.CustomerListWrapper;

/**
 * The class <code>CustomerListWrapperSerializer</code> is responsible to 
 * transform a <code>Cusotmer</code> object list in JSON.
 * .
 * @author Erik Paula
 * @version 1.0 03/10/2018
 */
@Component
public class CustomerListWrapperSerializer extends JsonSerializer<CustomerListWrapper>{
    
    private CustomerSerializer customerSerializar;

    @Override
    public void serialize(final CustomerListWrapper wrapper, final JsonGenerator gen, final SerializerProvider serializers)
            throws IOException, JsonProcessingException {
        final CrmJsonGenerator generator = new CrmJsonGenerator(gen);
        
        generator.writeStartObject();
        generator.writeArrayFieldStart(CUSTOMERS);
        for(final Customer customer: wrapper.getCustomers()) {
            customerSerializar.serialize(customer, generator);
        }
        generator.writeEndArray();
        generator.writeEndObject();
    }

    @Autowired
    public void setCustomerSerializer(final CustomerSerializer customerSerializer) {
        this.customerSerializar = customerSerializer;
    }
}
