/*
* Copyright 2018 Builders
*************************************************************
*Nome     : CustomerDeserializer.java
*Autor    : Erik Paula
*Data     : Mon Oct 02 2018 23:28:00 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.serializers;

import static br.com.builders.treinamento.serializers.SerializationLabel.BASE_URL;
import static br.com.builders.treinamento.serializers.SerializationLabel.CRM_ID;
import static br.com.builders.treinamento.serializers.SerializationLabel.LOGIN;
import static br.com.builders.treinamento.serializers.SerializationLabel.NAME;

import java.io.IOException;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;

import br.com.builders.treinamento.customer.Customer;
import br.com.builders.treinamento.wrappers.CustomerWrapper;

/**
 * The class <code>CustomerDeserializer</code> is responsible for deserialize
 * customer JSON.
 * 
 * @author Erik Paula
 * @version 1.0 02/10/2018
 */
@Component
public class CustomerDeserializer extends AbstractDeserializer<CustomerWrapper> {

    @Override
    public CustomerWrapper deserialize(final JsonParser jsonParser, final DeserializationContext context) throws IOException {
       final ObjectCodec objectCodec = jsonParser.getCodec();
       final JsonNode jsonNode = objectCodec.readTree(jsonParser);
       return deserialize(jsonNode);
    }

    protected CustomerWrapper deserialize(final JsonNode node) throws IOException {
        final Customer customer = new Customer();
        customer.setCrmId(getFieldTextValue(node, CRM_ID));
        customer.setBaseUrl(getFieldTextValue(node, BASE_URL));
        customer.setLogin(getFieldTextValue(node, LOGIN));
        customer.setName(getFieldTextValue(node, NAME));
        return new CustomerWrapper(customer);
    }

}
