/*
* Copyright 2018 Builders
*************************************************************
*Nome     : CrmJsonGenerator.java
*Autor    : Erik Paula
*Data     : Mon Oct 03 2018 23:28:00 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.serializers;

import static br.com.builders.treinamento.serializers.SerializationLabel.BASE_URL;
import static br.com.builders.treinamento.serializers.SerializationLabel.CRM_ID;
import static br.com.builders.treinamento.serializers.SerializationLabel.ID;
import static br.com.builders.treinamento.serializers.SerializationLabel.LOGIN;
import static br.com.builders.treinamento.serializers.SerializationLabel.NAME;

import java.io.IOException;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import br.com.builders.treinamento.customer.Customer;
import br.com.builders.treinamento.wrappers.CustomerWrapper;

/**
 * The class <code>CustomerSerializer</code> is responsible to 
 * transform <code>Cusotmer</code> objects in JSON.
 * .
 * @author Erik Paula
 * @version 1.0 03/10/2018
 */
@Component
public class CustomerSerializer extends JsonSerializer<CustomerWrapper>{

    @Override
    public void serialize(final CustomerWrapper customerWrapper, final JsonGenerator gen, final SerializerProvider serializers)
            throws IOException {
        final CrmJsonGenerator generator = new CrmJsonGenerator(gen);
        serialize(customerWrapper, generator);
        
    }

    protected void serialize(final CustomerWrapper customerWrapper, final CrmJsonGenerator generator) throws IOException {
        final Customer customer = customerWrapper.getCustomer();
        serialize(customer, generator);
        
    }
    
    protected void serialize(final Customer customer, final CrmJsonGenerator generator) throws IOException {
        generator.writeStartObject();
        generator.writeStringField(ID, customer.getId());
        generator.writeStringField(CRM_ID, customer.getCrmId());
        generator.writeStringField(BASE_URL, customer.getBaseUrl());
        generator.writeStringField(NAME, customer.getName());
        generator.writeStringField(LOGIN, customer.getLogin());
        generator.writeEndObject();
    }
    
}
