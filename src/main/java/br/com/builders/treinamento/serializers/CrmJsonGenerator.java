/*
* Copyright 2018 Builders
*************************************************************
*Nome     : CrmJsonGenerator.java
*Autor    : Erik Paula
*Data     : Mon Oct 03 2018 23:28:00 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.serializers;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;

import lombok.Getter;

/**
 * The class <code>CrmJsonGenerator</code> is a proxy to JsonGenerator
 * class and has the objective to handle how null fields will be
 * serialized.
 * .
 * @author Erik Paula
 * @version 1.0 03/10/2018
 */
@Getter
public class CrmJsonGenerator {
    
    private static final String WHITESPACE = "";

    private final JsonGenerator generator;
    private boolean writeNullable;

    public CrmJsonGenerator(final JsonGenerator generator) {
        this.generator = generator;
    }

    public CrmJsonGenerator(final JsonGenerator generator, final boolean writeNullable) {
        this.generator = generator;
        this.writeNullable = writeNullable;
    }

    public void writeStartObject() throws IOException {
        generator.writeStartObject();
    }

    public void writeObjectFieldStart(final SerializationLabel fieldName) throws IOException {
        generator.writeObjectFieldStart(fieldName.getLabelValue());
    }

    public void writeNumberField(final SerializationLabel fieldName, final Long value) throws IOException {
        if (value != null) {
            generator.writeNumberField(fieldName.getLabelValue(), value);
        } else if (writeNullable) {
            generator.writeStringField(fieldName.getLabelValue(), WHITESPACE);
        }
    }

    public void writeNumberField(final SerializationLabel fieldName, final Integer value) throws IOException {
        if (value != null) {
            generator.writeNumberField(fieldName.getLabelValue(), value);
        } else if (writeNullable) {
            generator.writeStringField(fieldName.getLabelValue(), WHITESPACE);
        }
    }

    public void writeStringField(final SerializationLabel fieldName, final String value) throws IOException {
        if (value != null) {
            generator.writeStringField(fieldName.getLabelValue(), value);
        } else if (writeNullable) {
            generator.writeStringField(fieldName.getLabelValue(), WHITESPACE);
        }
    }

    public void writeStringField(final SerializationLabel fieldName, final Enum<?> enumeratedValue) throws IOException {
        writeStringField(fieldName, enumeratedValue.name());
    }

    public void writeBooleanField(final SerializationLabel fieldName, final Boolean value) throws IOException {
        if (value != null) {
            generator.writeBooleanField(fieldName.getLabelValue(), value);
        } else if (writeNullable) {
            generator.writeStringField(fieldName.getLabelValue(), WHITESPACE);
        }
    }

    public void writeArrayFieldStart(final SerializationLabel fieldName) throws IOException {
        generator.writeArrayFieldStart(fieldName.getLabelValue());
    }

    public void writeString(final String text) throws IOException {
        generator.writeString(text);
    }

    public void writeNumber(final Long value) throws IOException {
        generator.writeNumber(value);
    }

    public void writeStartArray() throws IOException {
        generator.writeStartArray();
    }

    public void writeEndArray() throws IOException {
        generator.writeEndArray();
    }

    public void writeEndObject() throws IOException {
        generator.writeEndObject();
    }

}
