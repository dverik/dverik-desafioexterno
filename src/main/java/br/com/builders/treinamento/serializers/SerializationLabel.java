/*
* Copyright 2018 Builders
*************************************************************
*Nome     : SerializationLabel.java
*Autor    : Erik Paula
*Data     : Tue Oct 02 2018 11:00:00 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.serializers;

import lombok.Getter;

/**
 * The enum <code>SerializationLabel</code> gathering all the serialization
 * and deserialization constants used by application
 * 
 * @author Erik Paula
 * @version 1.0 02/10/2018
 */
@Getter
public enum SerializationLabel {
    
    BASE_URL("baseUrl"),
    CRM_ID("crmId"),
    CUSTOMERS("customers"),
    ID("id"),
    LOGIN("login"),
    NAME("name");

    private String labelValue;
    
    private SerializationLabel(final String labelValue) {
        this.labelValue = labelValue;
    }
}
