/*
* Copyright 2018 Builders
*************************************************************
*Nome     : AbstractDeserializer.java
*Autor    : Erik Paula
*Data     : Tue Oct 02 2018 11:00:00 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.serializers;

import java.io.IOException;
import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import br.com.builders.treinamento.exception.InvalidJsonException;

/**
 * The class <code>AbstractDeserializer</code> contains all the common code
 * for object deserialization in the application.
 * 
 * @author Erik Paula
 * @version 1.0 02/10/2018
 */
@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public abstract class AbstractDeserializer<T> extends JsonDeserializer<T> {
    
    @Override
    public T deserialize(final JsonParser jsonParser, final DeserializationContext context) throws IOException {
        final ObjectCodec oc = jsonParser.getCodec();
        final JsonNode node = oc.readTree(jsonParser);
        try {
            return deserialize(node);
        } catch (final Exception e) {
            throw new InvalidJsonException("\"Invalid input, object invalid\"", e);
        }
    }

    protected abstract T deserialize(JsonNode node) throws IOException;

    protected String getFieldTextValue(final JsonNode node, final SerializationLabel fieldName) {
        return node.has(fieldName.getLabelValue()) ? node.get(fieldName.getLabelValue()).textValue() : null;
    }

    protected Double getFieldDoubleValue(final JsonNode node, final SerializationLabel fieldName) {
        return node.has(fieldName.getLabelValue()) ? node.get(fieldName.getLabelValue()).doubleValue() : null;
    }

    protected Date getFieldDateValue(final JsonNode node, final SerializationLabel fieldName) {
        return hasNonNull(node, fieldName) ? new Date(node.get(fieldName.getLabelValue()).asLong()) : null;
    }

    protected Long getFieldLongValue(final JsonNode node, final SerializationLabel fieldName) {
        return node.has(fieldName.getLabelValue()) ? node.get(fieldName.getLabelValue()).asLong() : null;
    }

    protected Integer getFieldIntegerValue(final JsonNode node, final SerializationLabel fieldName) {
        return node.has(fieldName.getLabelValue()) ? node.get(fieldName.getLabelValue()).asInt() : null;
    }

    protected boolean getFieldBooleanValue(final JsonNode node, final SerializationLabel fieldName) {
        return node.has(fieldName.getLabelValue()) ? node.get(fieldName.getLabelValue()).asBoolean() : false;
    }

    protected boolean hasNonNull(final JsonNode node, final SerializationLabel fieldName) {
        return node.hasNonNull(fieldName.getLabelValue());
    }

    protected boolean has(final JsonNode node, final SerializationLabel fieldName) {
        return node.has(fieldName.getLabelValue());
    }

    protected JsonNode get(final JsonNode node, final SerializationLabel fieldName) {
        return node.get(fieldName.getLabelValue());
    }

    protected boolean isArray(final JsonNode node, final SerializationLabel fieldName) {
        return hasNonNull(node, fieldName) && get(node, fieldName).isArray();
    }

    protected boolean isEmptyArray(final JsonNode node, final SerializationLabel fieldName) {
        return isArray(node, fieldName) && get(node, fieldName).size() == 0;
    }

}
