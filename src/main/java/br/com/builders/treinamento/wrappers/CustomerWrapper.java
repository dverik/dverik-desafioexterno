/*
* Copyright 2018 Builders
*************************************************************
*Nome     : CustomerDeserializer.java
*Autor    : Erik Paula
*Data     : Mon Oct 02 2018 23:28:00 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.wrappers;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.builders.treinamento.customer.Customer;
import br.com.builders.treinamento.serializers.CustomerDeserializer;
import br.com.builders.treinamento.serializers.CustomerSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * The class <code>CustomerWrapper</code> is responsible for encapsulate
 * a Customer Object for serialization and deserialization.
 * 
 * @author Erik Paula
 * @version 1.0 02/10/2018
 */
@Getter
@AllArgsConstructor
@JsonDeserialize(using = CustomerDeserializer.class)
@JsonSerialize(using = CustomerSerializer.class)
public class CustomerWrapper {

    private final Customer customer;
}
