/*
* Copyright 2018 Builders
*************************************************************
*Nome     : CrmController.java
*Autor    : Erik Paula
*Data     : Mon Oct 01 2018 23:28:00 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/

package br.com.builders.treinamento.restapi;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.builders.treinamento.customer.CustomerServices;
import br.com.builders.treinamento.customer.CustomerValidator;
import br.com.builders.treinamento.exception.CustomerAlreadyExistisException;
import br.com.builders.treinamento.wrappers.CustomerListWrapper;
import br.com.builders.treinamento.wrappers.CustomerWrapper;

/**
 * The class <code>CrmController</code> is responsible to receive requests
 * referring to CRM resources. 
 * .
 * @author Erik Paula
 * @version 1.0 01/10/2018
 */
@RestController(value = "/api")
public class CrmController {

    private CustomerServices customerServices;
    
    @PostMapping(path = "/customers", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> createCustomer(@Valid @RequestBody final CustomerWrapper customerWrapper) throws CustomerAlreadyExistisException {
        return customerServices.createCustomer(customerWrapper.getCustomer());
    }
    
    @PutMapping(path = "/customers/{customerId}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> updateCustomer(@PathVariable("customerId") final String customerId, 
            @Valid @RequestBody CustomerWrapper customerWrapper) {
        return customerServices.updateCustomer(customerId, customerWrapper.getCustomer());
    }

    @PatchMapping(path = "/customers/{customerId}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> partialCustomerUpdate(@PathVariable("customerId") final String customerId, 
            @Valid @RequestBody CustomerWrapper customerWrapper) {
        return customerServices.partialCustomerUpdate(customerId, customerWrapper.getCustomer());
    }
    
    @DeleteMapping(path = "/customers/{customerId}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> deleteCustomer(@PathVariable("customerId") final String customerId) {
        return customerServices.deleteCustomer(customerId);
    }
    
    @GetMapping(path = "/customers", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<CustomerListWrapper> getCustomers() {
        return customerServices.getAllCustomers();
    }
    
    @GetMapping(path = "/customers/{customerId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<CustomerWrapper> getCustomer(@PathVariable("customerId") final String customerId) {
        return customerServices.getCustomer(customerId);
    }
    
    @Autowired
    public void setCustomerServices(final CustomerServices customerServices) {
        this.customerServices = customerServices;
    }
    
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new CustomerValidator());
    }
}
