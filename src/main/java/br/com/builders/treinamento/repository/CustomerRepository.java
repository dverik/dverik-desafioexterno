/*
* Copyright 2018 Builders
*************************************************************
*Nome     : CustomerRepository.java
*Autor    : Erik Paula
*Data     : Mon Oct 02 2018 23:28:00 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.builders.treinamento.customer.Customer;

/**
 * The interface <code>CustomerRepository</code> represents a repository
 * that handles persistance operations from Customer objects.
 * 
 * @author Erik Paula
 * @version 1.0 02/10/2018
 */
public interface CustomerRepository extends MongoRepository<Customer, String> {
    
    public Customer findByLogin(final String login);
    
    public Customer findById(final String customerId);

}
