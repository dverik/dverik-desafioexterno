/*
* Copyright 2018 Builders
*************************************************************
*Nome     : CustomerException.java
*Autor    : Erik Paula
*Data     : Mon Oct 02 2018 23:28:00 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.exception;

/**
 * The class <code>CustomerException</code> represents exceptions related
 * with Customers
 * .
 * @author Erik Paula
 * @version 1.0 03/10/2018
 */
public class CustomerException extends Exception {

    private static final long serialVersionUID = 1L;

    public CustomerException() {
        super();
    }
    
    public CustomerException(final String message) {
        super(message);
    }
    
    public CustomerException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
