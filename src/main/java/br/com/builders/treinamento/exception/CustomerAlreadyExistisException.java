/*
* Copyright 2018 Builders
*************************************************************
*Nome     : CustomerAlreadyExistisException.java
*Autor    : Erik Paula
*Data     : Mon Oct 02 2018 23:28:00 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The class <code>CustomerAlreadyExistisException</code> represents exceptions related
 * with Customers
 * .
 * @author Erik Paula
 * @version 1.0 03/10/2018
 */
@ResponseStatus(code = HttpStatus.CONFLICT, reason = "User with the following login already exists")
public class CustomerAlreadyExistisException extends CustomerException{

    private static final long serialVersionUID = 1L;

}
