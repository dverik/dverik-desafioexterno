/*
* Copyright 2018 Builders
*************************************************************
*Nome     : InvalidJsonException.java
*Autor    : Erik Paula
*Data     : Tue Oct 02 2018 11:00:00 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * The exception class <code>InvalidJsonException</code> represents an invalid
 * json that the application tried to deserialize.
 * 
 * @author Erik Paula
 * @version 1.0 02/10/2018
 */
@ResponseStatus(value=HttpStatus.BAD_REQUEST)
public class InvalidJsonException extends JsonProcessingException {
    
    private static final long serialVersionUID = 1L;

    public InvalidJsonException(final String msg) {
        super(msg);
    }
    
    public InvalidJsonException(final String msg, final Throwable throwable) {
        super(msg, throwable);
    }
}
