/*
* Copyright 2018 Builders
*************************************************************
*Nome     : CustomerDeserializer.java
*Autor    : Erik Paula
*Data     : Mon Oct 02 2018 23:28:00 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.customer;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The class <code>Customer</code> represents a customer in 
 * CRM system.
 * 
 * @author Erik Paula
 * @version 1.0 02/10/2018
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "erik_duarte")
@EqualsAndHashCode(of = {"id"})
public class Customer {

    @Id
    private String id;
    
    @NotEmpty
    private String crmId;
    
    @NotEmpty
    private String baseUrl;
    
    @NotEmpty
    private String name;
    
    @NotEmpty
    private String login;
}
