/*
* Copyright 2018 Builders
*************************************************************
*Nome     : CustomerDeserializer.java
*Autor    : Erik Paula
*Data     : Mon Oct 02 2018 23:28:00 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.customer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import br.com.builders.treinamento.repository.CustomerRepository;
import br.com.builders.treinamento.wrappers.CustomerListWrapper;
import br.com.builders.treinamento.wrappers.CustomerWrapper;

/**
 * The class <code>CustomerServices</code> gathering all operation support
 * methods from objects of type Customer
 * 
 * @author Erik Paula
 * @version 1.0 02/10/2018
 */
@Component
public class CustomerServices {

    private CustomerRepository customerRepository;

    public ResponseEntity<String> createCustomer(final Customer customer) {
        
        final Customer existCustomer = customerRepository.findByLogin(customer.getLogin());
        if(existCustomer != null) {
           return new ResponseEntity<String>("\"An existing item(login) already exists\"", HttpStatus.CONFLICT);
        }
        
        customerRepository.save(customer);
        return new ResponseEntity<String>("\"Item created\"", HttpStatus.CREATED);
    }
    
    public ResponseEntity<String> updateCustomer(final String customerId, final Customer customer) {
        return updateCustomerEntity(customerId, customer);
    }

    public ResponseEntity<String> partialCustomerUpdate(final String customerId, final Customer customer) {
        return updateCustomerEntity(customerId, customer);
    }
    
    public ResponseEntity<String> deleteCustomer(final String customerId) {
        final Customer customerToBeDeleted = customerRepository.findById(customerId);
        if(customerToBeDeleted != null) {
            customerRepository.delete(customerToBeDeleted);
            return new ResponseEntity<String>("\"Item deleted\"", HttpStatus.OK);
        }
        return new ResponseEntity<String>("\"The customer is not found\"", HttpStatus.NOT_FOUND);
    }
    
    public ResponseEntity<CustomerListWrapper> getAllCustomers() {
        final List<Customer> customers = customerRepository.findAll();
        final CustomerListWrapper customersList = new CustomerListWrapper(customers);
        return new ResponseEntity<CustomerListWrapper>(customersList, HttpStatus.OK);
    }
    
    public ResponseEntity<CustomerWrapper> getCustomer(final String customerId) {
        final Customer customer = customerRepository.findById(customerId);
        if(customer == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        final CustomerWrapper customerWrapper = new CustomerWrapper(customer);
        return new ResponseEntity<CustomerWrapper>(customerWrapper, HttpStatus.OK);
    }
    
    private ResponseEntity<String> updateCustomerEntity(final String customerId, final Customer customer) {
        final Customer customerToUpdate = customerRepository.findById(customerId);
        if(customerToUpdate != null 
                && customerToUpdate.getLogin().equals(customer.getLogin())) {
            customer.setId(customerId);
            customerRepository.save(customer);
            return new ResponseEntity<String>("\"Item replaced\"", HttpStatus.OK);
        }
        return new ResponseEntity<String>("\"The customer (or any of supplied IDs) is not found\"", HttpStatus.NOT_FOUND);
    }
    

    @Autowired
    public void setCustomerRepository(final CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

}
