/*
* Copyright 2018 Builders
*************************************************************
*Nome     : CustomerValidator.java
*Autor    : Erik Paula
*Data     : Mon Oct 03 2018 23:28:00 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.customer;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import br.com.builders.treinamento.wrappers.CustomerWrapper;

public class CustomerValidator implements Validator{

    @Override
    public boolean supports(Class<?> clazz) {
        return CustomerWrapper.class.equals(clazz);
    }

    @Override
    public void validate(final Object target, final Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "customer.crmId", "crmId cannot be null");
        ValidationUtils.rejectIfEmpty(errors, "customer.baseUrl", "baseUrl cannot be null");
        ValidationUtils.rejectIfEmpty(errors, "customer.name", "name cannot be null");
        ValidationUtils.rejectIfEmpty(errors, "customer.login", "login format");
    }

}
